﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Common.Data.Ado.Repositories.Mappers;
using Rhino.Mocks;
using System.Data.SqlClient;
using System.Data;

namespace Data.AdoCommon.Test.Repositories.Mappers {
    [TestClass]
    public class UnitTestPrimitiveScalarListMapper {

        private MockRepository _mocks;
        private const long LongData = 1;
        private const int IntData = 2;
        private const string StringData = "Data";

        [TestInitialize]
        public void TestInitialize() {
            _mocks = new MockRepository();
        }

        [ExpectedException(typeof(InvalidOperationException))]
        [TestMethod]
        public void TestMapNonPrimitive() {
            PrimitiveScalarListMapper<DateTime> mapper = new PrimitiveScalarListMapper<DateTime>();
        }

        [TestMethod]
        public void TestMapPrimitive() {
            PrimitiveScalarListMapper<long> mapper1 = new PrimitiveScalarListMapper<long>();
            PrimitiveScalarListMapper<string> mapper2 = new PrimitiveScalarListMapper<string>();
        }

        [TestMethod]
        public void TestMapStringRow() {

            PrimitiveScalarListMapper<string> mapper = new PrimitiveScalarListMapper<string>();

            IDataRecord dataRecord = _mocks.DynamicMock<SqlDataReader>();
            Expect.Call(dataRecord.GetValue(0)).Return(StringData);
            _mocks.ReplayAll();

            Assert.AreEqual(StringData, mapper.Map(dataRecord));

        }

        [TestMethod]
        public void TestMapLongRow() {

            PrimitiveScalarListMapper<long> mapper = new PrimitiveScalarListMapper<long>();

            IDataRecord dataRecord = _mocks.DynamicMock<SqlDataReader>();
            Expect.Call(dataRecord.GetValue(0)).Return(LongData);
            _mocks.ReplayAll();

            Assert.AreEqual(LongData, mapper.Map(dataRecord));

        }

        [TestMethod]
        public void TestMapIntRow() {

            PrimitiveScalarListMapper<int> mapper = new PrimitiveScalarListMapper<int>();

            IDataRecord dataRecord = _mocks.DynamicMock<SqlDataReader>();
            Expect.Call(dataRecord.GetValue(0)).Return(IntData);
            _mocks.ReplayAll();

            Assert.AreEqual(IntData, mapper.Map(dataRecord));

        }

    }

}