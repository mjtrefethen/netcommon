﻿using System;
using System.Data;

namespace Common.Data.Ado.Contexts {

    /// <summary>
    /// 
    /// </summary>
    public class AdoUnitOfWork : IUnitOfWork {

        private IDbTransaction _transaction;
        private readonly Action<AdoUnitOfWork> _rolledBack;
        private readonly Action<AdoUnitOfWork> _committed;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transaction"></param>
        /// <param name="rolledBack"></param>
        /// <param name="committed"></param>
        public AdoUnitOfWork(IDbTransaction transaction, Action<AdoUnitOfWork> rolledBack, Action<AdoUnitOfWork> committed) {
            Transaction = transaction;
            _transaction = transaction;
            _rolledBack = rolledBack;
            _committed = committed;
        }

        /// <summary>
        /// 
        /// </summary>
        public IDbTransaction Transaction { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose() {

            if (_transaction == null) {
                return;
            }

            _transaction.Rollback();
            _transaction.Dispose();
            _rolledBack(this);
            _transaction = null;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Commit() {

            if (_transaction == null) {
                throw new InvalidOperationException("May not call save changes twice.");
            }

            _transaction.Commit();
            _committed(this);
            _transaction = null;
        }

    }

}
