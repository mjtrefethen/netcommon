﻿using System;

namespace Common.Data.Ado.Contexts {

    /// <summary>
    /// 
    /// </summary>
    public interface IUnitOfWork : IDisposable {

        /// <summary>
        /// 
        /// </summary>
        void Commit();

    }

}
