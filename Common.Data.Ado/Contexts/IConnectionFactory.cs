﻿using System.Data;

namespace Common.Data.Ado.Contexts {

    /// <summary>
    /// 
    /// </summary>
    public interface IConnectionFactory
    {
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IDbConnection Create();
    
    }

}
